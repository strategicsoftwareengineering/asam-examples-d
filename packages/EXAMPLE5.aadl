--Model for Architecture Safety Analysis Method (ASAM)-Annex
--Clemson University/School of Computing/Strategic Software Engineering Group(SSEG)
--https://bitbucket.org/strategicsoftwareengineering/asam-example

--Scenario-4)Isolette can be described as an infant incubator temperature control. 
--What will happen if the operator interface in an infant incubator gives 
--an (out of range value) for the air temperature inside the isolette because of an internal failure?

--Note: to run the example: Select Isolette.impl->R-click on it-> Choose ASAM->Run ASAM Report 

package EXAMPLE5
public
	
	system Operator_Interface
		features
			lower_upper_temp: out event data port;
			alarm_status: in event data port;
		flows
			f_source: flow source lower_upper_temp;
	end Operator_Interface;
	
	system implementation Operator_Interface.impl
		annex asam {**
			Errs => [{
				--Identify specific error type for the operator_interface, identify chance of harm people. 
				Type =>OutOfRange(Critical,p=0.0000001), 
				
				--For the specific error type, what kind of unsafe action will the operator_interface do?
				UCA => UCA1: "The operator_interface sends out of range error values to thermostat",
				Causes => {
					
					--What are the causes of the Unsafe Control Action(UCA) of the operator_interface?
					General => "The operator_interface has an internal failure",
					Specific => "The operator_interface recieves incorrect values from thermostat"
				},
				--Identify Safety Constraints (SC) for the operator_interface to mitigate effect of the UCA.
				SC => SC1: "The operator_interface should not send incorrect values to the thermostat"
				
			}]
	    **};
	end Operator_Interface.impl;
	
	system Thermostat
		features
			current_temp: in event data port;
			heat_control: out event data port;
			lower_upper_temp: in event data port;
			alarm_status: out event data port;
		flows
			f_path_1: flow path lower_upper_temp->heat_control;
			--f_path_2: flow path current_temp->alarm_status;
	end Thermostat;
	
	system implementation Thermostat.impl
		annex asam {**
	    	Errs => [{
	    		--Identify specific error type for the thermostat, identify chance of harm people.
				Type => AboveRange(Catastrophic,p=1),
				
				--For the specific error type, what kind of unsafe action will the thermostat do?
				UCA => UCA2: "Thermostat sends inadequate command (too hot cmd) to source of the heat 
				             and does not send message to warn the nurse",
				
				--What are the causes of the Unsafe Control Action(UCA) of the thermostat?
				Causes => {
					General => "The thermostat have an internal failure",
					Specific => "The thermostat receives above of the range values from the operator_interface"
				},
				
				--Identify Safety Constraints (SC) for the thermostat to mitigate effect of the UCA.
				SC => SC2: "Thermostat should send the message to warn the nurse at any out of range value"
				
			}]
	    **};
	end Thermostat.impl;
	
	system Heat_Source
		features
			heat_control: in event data port;
			heat: out event data port;
		flows
			f_path_3: flow path heat_control->heat;
	end Heat_Source;
	
	system implementation Heat_Source.impl
		annex asam {**
	    	Errs => [{
				--Identify specific error type for the Heat_source, identify chance of harm people.
				Type => AboveRange(Marginal,p=0.00001),
				
				--For the specific error type, what kind of unsafe action will the heat_source do?
				UCA => UCA3: "Heat_Source executes the too hot command",
				Causes => {
					--What are the causes of the Unsafe Control Action(UCA) of the heat_source?
					General => "The heat_source has an internal failure",
					Specific => "The heat_source received inadequate command from the thermostat"
				},
				--Identify Safety Constraints (SC) for the heat_source to mitigate effect of the UCA.
				SC => SC3: "Heat_Source must not execute too hot / too cold command when it receives error values (OutOfRange/Above/Below)"
				
			}]
	    **};
	end Heat_Source.impl;
	
	system Air
		features
			heat_in: in event data port;
			heat_out: out event data port;
		flows
			f_sink: flow sink heat_in;
	end Air;
	
	system implementation Air.impl
	end Air.impl;
	
	system Temperature_Sensor
		features
			current_temp: out event data port;
			update_temp: in event data port;
	end Temperature_Sensor;
	
	system implementation Temperature_Sensor.impl
	end Temperature_Sensor.impl;
	
	
	system Isolette
	end Isolette;
	
	system implementation Isolette.impl
		subcomponents
			OPERATOR_INTERFACE: system Operator_Interface.impl;
			THERMOSTAT: system Thermostat.impl;
			HEAT_SOURCE: system Heat_Source.impl;
			AIR: system Air.impl;
			TEMPRATURE_SENSOR: system Temperature_Sensor.impl;
		connections
			c0: port TEMPRATURE_SENSOR.current_temp -> THERMOSTAT.current_temp;
			c1: port THERMOSTAT.heat_control -> HEAT_SOURCE.heat_control;
			c2: port HEAT_SOURCE.heat -> AIR.heat_in;
			c3: port AIR.heat_out -> TEMPRATURE_SENSOR.update_temp;
			c4: port OPERATOR_INTERFACE.lower_upper_temp -> THERMOSTAT.lower_upper_temp;
			c5: port THERMOSTAT.alarm_status -> OPERATOR_INTERFACE.alarm_status;
		flows
			etoef: end to end flow OPERATOR_INTERFACE.f_source->c4->THERMOSTAT.f_path_1->c1->HEAT_SOURCE.f_path_3->c2->AIR.f_sink;
	end Isolette.impl;
	
	
	
	
	
end EXAMPLE5;
